
from random import random
import re
from typing import Any, Text, Dict, List
from unicodedata import name
from attr import attributes
from matplotlib.pyplot import cla
from rasa_sdk.types import DomainDict
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher

from actions.course import get_course


class ValidateClothesForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_change_account_form"

    def validate_phone_account(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `phone_account` value."""  
        value = tracker.get_slot("phone_account")
        print(f"Phone number = {slot_value} length = {len(slot_value)}")
        if re.search(r"(^(84|0[3|5|7|8|9])+([0-9]{8})\b)", slot_value):
            if len(value) <10 or len(value) >10:
                print("print id")
                dispatcher.utter_message(text="valid")
                return{"phone_account": value}
            else:
                print("nfhhf")
        else:
            dispatcher.utter_message(text=f"Số điện thoại của bạn đang không đúng! Mời bạn nhập lại!!")  
            if len(slot_value) > 10 or len (slot_value) < 10:
                dispatcher.utter_message(text=f"SDT không đúng")
            return {"phone_account": None}
     
        return {"phone_account": slot_value}

    
    def validate_email_account(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `email_account` value.""" 
        # nhungnguyen899@gmail.com
        value = tracker.get_slot("email_account")
        if re.search(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9]+\.[a-zA-Z0-9.]*\.*[com|org|edu]{3}$)",slot_value):
            if len(value) <6 or len(value) >255:
                print("print id")
                dispatcher.utter_message(text="valid")
                return{"email_account": value}
            else:
                print("nfhhf")
        else:
            print("inside")
            dispatcher.utter_message(text="Địa chỉ email của quý khách hiện tại không đúng!Xin quý khách nhập lại địa chỉ email!")
            return {"email_account": None}


class ActionDefine(Action):

    def name(self) -> Text:
        return "action_define"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "define"))
                dispatcher.utter_message(
                    text=get_course(name, "define"))

            else:
                dispatcher.utter_message(
                    template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []


class ActionFabricMaterial(Action):
    def name(self) -> Text:
        return "action_fabric_material"

    def run(self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
       
       try:
           name = tracker.get_slot("fashions")
           print(name)

           if name is not None:
               print(get_course(name, "fabric_material"))
               dispatcher.utter_message(text=get_course(name, "fabric_material"))
           else:
               dispatcher.utter_message(template="utter_no_fashions", attribute="")
       except Exception as e:
           print(str(e))
       return []


class ActionColor(Action):
    def name(self) -> Text:
        return "action_color"

    def run(self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
       
       try:
           name = tracker.get_slot("fashions")
           print(name)

           if name is not None:
               print(get_course(name, "color"))
               dispatcher.utter_message(text=get_course(name, "color"))
           else:
               dispatcher.utter_message(template="utter_no_fashions", attribute="")
       except Exception as e:
           print(str(e))
       return []

class ActionStyle(Action):
    def name(self) -> Text:
        return "action_style"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "style"))
                dispatcher.utter_message(text=get_course(name, "style"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionModel(Action):
    def name(self) -> Text:
        return "action_model"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "model"))
                dispatcher.utter_message(text=get_course(name, "model"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionPicture(Action):
    def name(self) -> Text:
        return "action_picture"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "picture"))
                dispatcher.utter_message(text=get_course(name, "picture"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionSize(Action):
    def name(self) -> Text:
        return "action_size"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "size"))
                dispatcher.utter_message(text=get_course(name, "size"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionPrice(Action):
    def name(self) -> Text:
        return "action_price"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "price"))
                dispatcher.utter_message(text=get_course(name, "price"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionHigh(Action):
    def name(self) -> Text:
        return "action_high"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "high"))
                dispatcher.utter_message(text=get_course(name, "high"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionWideaccessoryes(Action):
    def name(self) -> Text:
        return "action_wide_glasses"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "wide_glasses"))
                dispatcher.utter_message(text=get_course(name, "wide_glasses"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionAccessoryBridge(Action):
    def name(self) -> Text:
        return "action_glass_bridge"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "glass_bridge"))
                dispatcher.utter_message(text=get_course(name, "glass_bridge"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []

class ActionFrameLength(Action):
    def name(self) -> Text:
        return "action_frame_length"

    def run (self, dispatcher: "CollectingDispatcher", tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        try:
            name = tracker.get_slot("fashions")
            print(name)

            if name is not None:
                print(get_course(name, "frame_length"))
                dispatcher.utter_message(text=get_course(name, "frame_length"))
            else:
                dispatcher.utter_message(template="utter_no_fashions", attribute="")
        except Exception as e:
            print(str(e))
        return []


# add buttons

class  ActionButtonFashion(Action):
    def name(self) -> Text:
        return "action_button_fashion"

    def run(self, dispatcher: CollectingDispatcher, 
            tracker: Tracker, 
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        buttons_fashions = [
            {"payload": '/sample_costumes{fashion_type: "sample_costumes"}', "title": "Trang phục"},
            {"payload": '/sample_accessories{fashion_type: "sample_accessories"}', "title": "Phụ kiện"},
        ]

        dispatcher.utter_message(text="Quý khách muốn mua trang phục hay phụ kiện?", buttons=buttons_fashions)

        return []


# buttons trang phuc and phu kien

class ActionInforCostume(Action):
    def name(self) -> Text:
        return "action_costume"

    def run(self, dispatcher: CollectingDispatcher, 
            tracker: Tracker,
            domain:  Dict[Text, Any]) -> List[Dict[Text, Any]]:

        costume_list = []
        costume0 = {
            "name": "Dazy đầm buộc",
            "image_url": "https://cdn.kkfashion.vn/13507-large_default/dam-trang-chu-a-co-v-dinh-cuc-hl18-39.jpg?w=480",
            "price": "500.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume0)

        costume2 = {
            "name": "Đầm sơ mi",
            "image_url": "https://cdn.kkfashion.vn/8981-large_default/dam-so-mi-hoa-tiet-ke-phoi-no-kk107-26.jpg?w=480",
            "price": "530.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume2)
        costume3 = {
            "name": "Đầm hoa nhí",
            "image_url": "https://cdn.kkfashion.vn/12803-large_default/dam-hoa-nhi-vien-beo-tung-vay-xep-tang-kk111-01.jpg?w=480",
            "price": "600.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume3)

        costume5 = {
            "name": "Áo blazer",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/01/13/1642055291aab457dc3510e92b1af4eb379b371b86_thumbnail_600x.webp",
            "price": "470.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume5)
        costume6 = {
            "name": "Áo sơ mi công sở nữ tay lỡ",
            "image_url": "https://cdn.kkfashion.vn/7034-large_default/ao-so-mi-cong-so-nu-tay-lo-asm05-14.jpg?w=480",
            "price": "300.000đ",
            "link":"https://shevietnam.com.vn/"
        }
        costume_list.append(costume6)
        costume7 = {
            "name": "Áo kiểu nữ cổ đổ tay dài",
            "image_url": "https://mcdn.nhanh.vn/store3/108661/ps/20211130/DSC_8681_thumb.jpg",
            "price": "490.000đ",
            "link": "https://jm.com.vn/"
        }
        costume_list.append(costume7)
        costume8 = {
            "name": "Quần short",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2021/09/18/163194763446cdb37b735f343a128b9e5e0f1745b9_thumbnail_405x552.webp",
            "price": "450.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume8)
        costume9 = {
            "name": "Quần tây nữ công sở đai cách điệu",
            "image_url": "https://cdn.kkfashion.vn/12685-large_default/quan-tay-nu-cong-so-dai-cach-dieu-qcs03-03.jpg?w=480",
            "price": "320.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume9)
        costume10 = {
            "name": "Chân váy MIDI",
            "image_url": "https://cdn.kkfashion.vn/11775-large_default/chan-vay-midi-dang-chu-a-cv04-04.jpg?w=480",
            "price": "350.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume10)
        costume11 = {
            "name": "Glowmode Legging",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2021/06/30/16250442465aa72f7784165e8db352e90ff0d53ab6_thumbnail_405x552.webp",
            "price": "460.000đ",
            "link": "https://shevietnam.com.vn/"
        }
        costume_list.append(costume11)

        template_items = []
        for costume in costume_list:
            template_item = {
                "title": costume['name'],
                "image_url": costume['image_url'],
                "subtitle": costume['price'],
                "default_action": {
                    "type": "web_url",
                    "url": costume['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": costume['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu trang phục dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []

class ActionInforAccessory(Action):
    def name(self) -> Text:
        return "action_accessory"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        shirt_list = []
        shirt0 = {
            "name": "Balo phối ngăn mini",
            "image_url": "https://product.hstatic.net/1000003969/product/xanh-la_bl132_1_66c4d70641d74629a22c858774c1dc89_grande.jpg",
            "price": "780.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt0)
        shirt1 = {
            "name": "Túi xách nhỏ đeo chéo Pretty Nerdy",
            "image_url": "https://product.hstatic.net/1000003969/product/hong-nhat_txn533_4_26179386cd264ed7922750587a8ed10e_master.jpg",
            "price": "690.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt1)
        shirt2 = {
            "name": "Balo phối khoá kéo thân trước",
            "image_url": "https://product.hstatic.net/1000003969/product/xanh_bl129_1_507154c0f66f4fdcba585418eeaa3265_grande.jpg",
            "price": "890.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt2)

        shirt4 = {
            "name": "Giày cao gót Slingback",
            "image_url": "https://product.hstatic.net/1000003969/product/trang-kem_cg07108_5_715060f75ee647fdb82530daabc83dd7_master.jpg",
            "price": "470.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt4)

        shirt5 = {
            "name": "Giày búp bê quai chéo gắn khoá hoa trang trí",
            "image_url": "https://product.hstatic.net/1000003969/product/xanh-la_bb03086_3_d04d091c50c643538c361ce22c9cf787_master.jpg",
            "price": "450.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt5)

        shirt6 = {
            "name": "Giày búp bê mũi nhọn quai cách điệu",
            "image_url": "https://product.hstatic.net/1000003969/product/hong_bb03082_3_5f21b24e67a34e548e42a759f26ee46f_master.jpg",
            "price": "380.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt6)

        shirt9 = {
            "name": "Giày cao gót dây",
            "image_url": "https://product.hstatic.net/1000003969/product/xanh_cg07103_3_e814130c90c04a0283267a7fd21cce37_master.jpg",
            "price": "446.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt9)

        shirt7 = {
            "name": "Sneakers Phoenix",
            "image_url": "https://product.hstatic.net/1000003969/product/bac_tt05008_3_12e4ed322ea04d3e87fa71061cc92e3d_master.jpg",
            "price": "880.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt7)

        shirt55 = {
            "name": "Mắt kính vuông gọng nhựa",
            "image_url": "https://product.hstatic.net/1000003969/product/den_mk071_4_ccea4e4dfbf9455aa05995ca45c098e4_master.jpg",
            "price": "446.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt55)

        shirt12 = {
            "name": "Mắt kính vuông kim loại trang trí viền",
            "image_url": "https://product.hstatic.net/1000003969/product/nau-do_mk066_4_4ff47b4da1b44b56b7319fff8a7557aa_master.jpg",
            "price": "446.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt12)
        
    
        template_items = []
        for shirt in shirt_list:

            template_item = {
                "title": shirt['name'],
                "image_url": shirt['image_url'],
                "subtitle": shirt['price'],
                "default_action": {
                    "type": "web_url",
                    "url": shirt['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": shirt['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu phụ kiện dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []


# add buttons style

class  ActionButtonsStyle(Action):
    def name(self) -> Text:
        return "action_button_style"

    def run(self, dispatcher: CollectingDispatcher, 
            tracker: Tracker, 
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        style_list = []
        body0 = {
            "name": "Phong cách cổ điển",
            "image_url": "https://www.haydocla.com/wp-content/uploads/2021/07/thoi-trang-phong-cach-co-dien-6.jpg",
            "link_confirm": "https://sovintage.com.vn/",
            "link_mix": "https://www.haydocla.com/thoi-trang-phong-cach-co-dien",
        }
        style_list.append(body0)

        body1 = {
            "name": "Phong cách minimalist",
            "image_url": "https://dienmayxuyena.com/phong-cach-thoi-trang-nu/imager_3_34353_700.jpg",
            "link_confirm": "http://kidostudiowear.com/",
            "link_mix": "https://www.elle.vn/xu-huong-phong-cach/minimalism-goi-y-phoi-do-dep-theo-phong-cach-toi-gian",
        }
        style_list.append(body1)

        body2 = {
            "name": "Phong cách tự do",
            "image_url": "https://i.pinimg.com/564x/b5/52/3e/b5523e33c7f42f7d9b2d556a03b80f7d.jpg",
            "link_confirm": "https://marc.com.vn/",
            "link_mix": "https://luvinus.com/cam-nang-meo-hay/cach-phoi-do-nu-di-choi/",
        }
        style_list.append(body2)

        body3 = {
            "name": "Phong cách boho",
            "image_url": "https://i.pinimg.com/564x/c0/07/f4/c007f4a60fe530c83ed4faedcee8888a.jpg",
            "link_confirm": "http://kidostudiowear.com/",
            "link_mix": "https://www.elle.vn/xu-huong-phong-cach/thoi-trang-cong-so-theo-phong-cach-boho",
        }
        style_list.append(body3)

        body4 = {
            "name": "Phong cách Normcore giản dị",
            "image_url": "https://i.pinimg.com/564x/bc/5d/84/bc5d844c18875f78a8998f1db6b40981.jpg",
            "link_confirm": "https://marc.com.vn/",
            "link_mix": "https://gtvietnam.com/thoi-trang-normcore-gian-di-va-don-gian-den-bat-ngo/",
        }
        style_list.append(body4)

    
        template_items = []
        for body in style_list:

            template_item = {
                "title": body['name'],
                "image_url": body['image_url'],
                "default_action": {
                    "type": "web_url",
                    "url": body['link_confirm'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": body['link_confirm'],
                        "title": "Shop"
                    },
                    {
                        "type": "web_url",
                        "url": body['link_mix'],
                        "title": "Cách mix theo style"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các style và cách mix đồ:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []    

# add buttons shape body


class ActionInforRectangleBody(Action):
    def name(self) -> Text:
        return "action_shape_body"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        body_list = []
        body0 = {
            "name": "Dáng người hình chữ nhật",
            "image_url": "https://global-uploads.webflow.com/5eca30fd2b50b671e2107b06/5ed91e4d48dbc1c6ecae6e78_Rectangle%20Body%20Shape%20Sleeves%20Do's%20and%20Don'ts.png",
            "link_confirm": "https://www.wikihow.vn/X%C3%A1c-%C4%91%E1%BB%8Bnh-h%C3%ACnh-d%C3%A1ng-c%C6%A1-th%E1%BB%83",
            "link_mix": "https://www.kkfashion.vn/blog/cach-chon-hoa-tiet-phu-hop-voi-5-dang-nguoi-co-ban",
        }
        body_list.append(body0)

        body1 = {
            "name": "Dáng người hình quả táo",
            "image_url": "https://global-uploads.webflow.com/5eca30fd2b50b671e2107b06/5edbe06ce93ebb0090407da1_5ed3a2e9ec5df28c32f8f33a_Apple-Body-Shape-Necklines-Dos-and-Donts.jpeg",
            "link_confirm": "https://www.wikihow.vn/X%C3%A1c-%C4%91%E1%BB%8Bnh-h%C3%ACnh-d%C3%A1ng-c%C6%A1-th%E1%BB%83",
            "link_mix": "https://www.kkfashion.vn/blog/cach-chon-hoa-tiet-phu-hop-voi-5-dang-nguoi-co-ban",
        }
        body_list.append(body1)

        body2 = {
            "name": "Dáng người hình quả lê",
            "image_url": "https://i.pinimg.com/originals/e9/c6/93/e9c693f98c9af19c75964025768458e2.jpg",
            "link_confirm": "https://www.wikihow.vn/X%C3%A1c-%C4%91%E1%BB%8Bnh-h%C3%ACnh-d%C3%A1ng-c%C6%A1-th%E1%BB%83",
            "link_mix": "https://www.kkfashion.vn/blog/cach-chon-hoa-tiet-phu-hop-voi-5-dang-nguoi-co-ban",
        }
        body_list.append(body2)

        body3 = {
            "name": "Dáng người tam giác ngược",
            "image_url": "https://i.pinimg.com/originals/6c/58/6a/6c586a18d5b4efdc7e31e099304e8532.jpg",
            "link_confirm": "https://www.wikihow.vn/X%C3%A1c-%C4%91%E1%BB%8Bnh-h%C3%ACnh-d%C3%A1ng-c%C6%A1-th%E1%BB%83",
            "link_mix": "https://www.kkfashion.vn/blog/cach-chon-hoa-tiet-phu-hop-voi-5-dang-nguoi-co-ban",
        }
        body_list.append(body3)

        body4 = {
            "name": "Dáng người đồng hồ cát",
            "image_url": "https://th.bing.com/th/id/OIP.DyRm_6F1LVJPNRhl8YGbWAHaHa?pid=ImgDet&rs=1",
            "link_confirm": "https://www.wikihow.vn/X%C3%A1c-%C4%91%E1%BB%8Bnh-h%C3%ACnh-d%C3%A1ng-c%C6%A1-th%E1%BB%83",
            "link_mix": "https://www.kkfashion.vn/blog/cach-chon-hoa-tiet-phu-hop-voi-5-dang-nguoi-co-ban",
        }
        body_list.append(body4)

    
        template_items = []
        for body in body_list:

            template_item = {
                "title": body['name'],
                "image_url": body['image_url'],
                "default_action": {
                    "type": "web_url",
                    "url": body['link_confirm'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": body['link_confirm'],
                        "title": "Các xác định hình dáng"
                    },
                    {
                        "type": "web_url",
                        "url": body['link_mix'],
                        "title": "Cách mix đồ theo dáng"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các hình dáng cơ bản của dáng người và cách mix đồ:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []


class ActionSegmentClothes15(Action):
    def name(self) -> Text:
        return "action_segment_clothes15"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        shirt_list = []
        shirt0 = {
            "name": "Dazy đầm viền lá sen",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/05/23/1653275473b614b73102cb6778013475e1aac008fe_thumbnail_405x552.webp",
            "price": "419.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt0)
        shirt1 = {
            "name": "Đầm thắt nút",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2021/05/24/162184232798d4fc83e4be31eb06f25cd5af2c25a4_thumbnail_405x552.webp",
            "price": "375.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt1)
        shirt2 = {
            "name": "Đầm thắt nút dải trun",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/01/14/1642149618d3ef00e137d77bab6777501e67d290bd_thumbnail_405x552.webp",
            "price": "500.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt2)

        shirt4 = {
            "name": "Đầm xoè xếp ly",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/05/05/1651740070bb99c79fbfc7766947ceabc791b1040c_thumbnail_405x552.webp",
            "price": "470.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt4)
    
        template_items = []
        for shirt in shirt_list:

            template_item = {
                "title": shirt['name'],
                "image_url": shirt['image_url'],
                "subtitle": shirt['price'],
                "default_action": {
                    "type": "web_url",
                    "url": shirt['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": shirt['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []

class ActionSegmentClothes510(Action):
    def name(self) -> Text:
        return "action_segment_clothes510"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        shirt_list = []
        shirt0 = {
            "name": "Dazy đầm xếp nút",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/05/17/16527701197f4ab26e4851244905193a7118151f91_thumbnail_405x552.webp",
            "price": "560.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt0)
        shirt1 = {
            "name": "Đầm trơn",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/05/23/1653272277931f6f0c5540366ea1f1a79ee9c9d299_thumbnail_405x552.webp",
            "price": "700.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt1)
        shirt2 = {
            "name": "Đầm thắt nút trơn",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2021/07/15/1626318759c9b3369189f0cf8a885d4a20db448cdf_thumbnail_405x552.webp",
            "price": "660.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt2)

        shirt4 = {
            "name": "Đầm hoa nhí",
            "image_url": "https://img.ltwebstatic.com/images3_pi/2022/04/26/1650958261cd313a8305ae1c76f5536f23fb092498_thumbnail_405x552.webp",
            "price": "890.000đ",
            "link": "https://www.shein.com.vn/",
        }
        shirt_list.append(shirt4)
    
        template_items = []
        for shirt in shirt_list:

            template_item = {
                "title": shirt['name'],
                "image_url": shirt['image_url'],
                "subtitle": shirt['price'],
                "default_action": {
                    "type": "web_url",
                    "url": shirt['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": shirt['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []


class ActionSegmentAccessory15(Action):
    def name(self) -> Text:
        return "action_segment_accessory15"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        shirt_list = []
        shirt0 = {
            "name": "Giày Mules",
            "image_url": "https://product.hstatic.net/1000003969/product/kem_bb03093_7_b83d37a194584b7692f80f308641a2fc_grande.jpg",
            "price": "446.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt0)
        shirt1 = {
            "name": "Giày cao gót",
            "image_url": "https://product.hstatic.net/1000003969/product/xam-tim_cg07109_7_b18652a8f76e41f195467e28b9c87dc9.jpg",
            "price": "446.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt1)
        shirt2 = {
            "name": "Giày búp bê",
            "image_url": "https://product.hstatic.net/1000003969/product/den_bb03084_7_0c42d99341b349e797a23ec3589fe5f2.jpg",
            "price": "395.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt2)

        shirt4 = {
            "name": "Giày mũi vuông",
            "image_url": "https://product.hstatic.net/1000003969/product/xam-tim_bb03080_7_045a176eb1554570ade7351367b58a71_grande.jpg",
            "price": "500.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt4)
    
        template_items = []
        for shirt in shirt_list:

            template_item = {
                "title": shirt['name'],
                "image_url": shirt['image_url'],
                "subtitle": shirt['price'],
                "default_action": {
                    "type": "web_url",
                    "url": shirt['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": shirt['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []

class ActionSegmentAccessory510(Action):
    def name(self) -> Text:
        return "action_segment_accessory510"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        shirt_list = []
        shirt0 = {
            "name": "Túi xách đeo chéo",
            "image_url": "https://product.hstatic.net/1000003969/product/xanh-la_txn575_7_408a93b5fa694415b3115c3fe9d4b5e2_grande.jpg",
            "price": "643.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt0)
        shirt1 = {
            "name": "Ví kèm khoá",
            "image_url": "https://product.hstatic.net/1000003969/product/kem_vi171_7_574db27dacd74fa7b41a30da9ba15528_grande.jpg",
            "price": "700.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt1)
        shirt2 = {
            "name": "Túi xách nhỏ",
            "image_url": "https://product.hstatic.net/1000003969/product/kem_txn551_7_84e7c22eecc348d5899e89bf58f8996b_grande.jpg",
            "price": "893.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt2)

        shirt4 = {
            "name": "Túi xách nắp gập",
            "image_url": "https://product.hstatic.net/1000003969/product/vang_txn488_7_87369f1522054f81bf576a5d26d92c11_grande.jpg",
            "price": "741.000đ",
            "link": "https://juno.vn/",
        }
        shirt_list.append(shirt4)
    
        template_items = []
        for shirt in shirt_list:

            template_item = {
                "title": shirt['name'],
                "image_url": shirt['image_url'],
                "subtitle": shirt['price'],
                "default_action": {
                    "type": "web_url",
                    "url": shirt['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": shirt['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các mẫu dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []


class ActionMixColor(Action):
    def name(self) -> Text:
        return "action_mix_color"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        color_list = []
        color0 = {
            "name": "Calming Coral",
            "image_url": "https://vareno.vn/wp-content/uploads/2021/11/color-trends-2022-2.jpg",
            "define": "Màu hồng đào dịu nhẹ, phù hợp với màu vàng và hồng bụi tạo sắc thái hoài cổ cho thiết kế.",
            "link": "https://vareno.vn/en/xu-huong-mau-sac-color-trends-2022.html",
        }
        color_list.append(color0)
        color1 = {
            "name": "Velvet Violet",
            "image_url": "https://vareno.vn/wp-content/uploads/2021/11/color-trends-2022-4.jpg",
            "define": "Màu sắc mang đến sự vương giả, tự tin vào sự duyên dáng và quyến rũ mà nó mang lại.",
            "link": "https://vareno.vn/en/xu-huong-mau-sac-color-trends-2022.html",
        }
        color_list.append(color1)
        color2 = {
            "name": "Pacific Pink",
            "image_url": "https://vareno.vn/wp-content/uploads/2021/11/color-trends-2022-6.jpg",
            "define": "Màu hồng của những bông hoa hồng tạo cảm giác dịu mát, thu hút và ấn tượng, lôi cuốn.",
            "link": "https://vareno.vn/en/xu-huong-mau-sac-color-trends-2022.html",
        }
        color_list.append(color2)

        color3 = {
            "name": "Green reigns supreme",
            "image_url": "https://vareno.vn/wp-content/uploads/2021/11/color-trends-2022-7.jpg",
            "define": "Tỷ lệ thành công của thiết kế quảng cáo khi sử dụng tone màu này, màu xanh lá cây chiếm ưu thế.",
            "link": "https://vareno.vn/en/xu-huong-mau-sac-color-trends-2022.html",
        }
        color_list.append(color3)
    
        template_items = []
        for color in color_list:

            template_item = {
                "title": color['name'],
                "image_url": color['image_url'],
                "subtitle": color['define'],
                "default_action": {
                    "type": "web_url",
                    "url": color['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": color['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các màu trend dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []

class ActionMixOffice(Action):
    def name(self) -> Text:
        return "action_mix_office"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # These should be get from server , do not do like this :D
        color_list = []
        color0 = {
            "name": "Màu Nude",
            "image_url": "https://fashiioncarpet.com/wp-content/uploads/2017/08/Nina-schwichtenberg-fashiioncarpet-haar-trends-2017-proenza-schouler-ps-11-fashion-blog-m%C3%BCnchen-800x1198.jpg",
            "define": "Bởi màu da vô cùng mềm mại, trang nhã lại có phần sang trọng, quý phái tạo nên vẻ sang trọng",
            "link": "https://vts.edu.vn/6-cach-phoi-mau-thoi-trang-cong-so-hot-nhat/",
        }
        color_list.append(color0)
        color1 = {
            "name": "Màu Nâu",
            "image_url": "https://cdn.shopify.com/s/files/1/0274/5054/0104/products/WSUM21T13-SIENN-Courtney-Short-Sleeve-Button-Down-Shirt-Sienna-Fig_Shot1_2221_400x.jpg?v=1626298518",
            "define": "Gam màu này cực kỳ phù hợp với sự sang trọng và thanh lịch của môi trường công sở.",
            "link": "https://vts.edu.vn/6-cach-phoi-mau-thoi-trang-cong-so-hot-nhat/",
        }
        color_list.append(color1)
        color2 = {
            "name": "Màu đen",
            "image_url": "https://cdn.shopify.com/s/files/1/0274/5054/0104/products/FebruaryMarch-WSPR22D32-Black-DrewChiffonPlisseDressWithSideCutouts-Fig_3664_400x.jpg?v=1649116370",
            "define": "Màu đen tạo cảm giác dịu mát, thu hút và ấn tượng, lôi cuốn.",
            "link": "https://vts.edu.vn/6-cach-phoi-mau-thoi-trang-cong-so-hot-nhat/",
        }
        color_list.append(color2)

        color3 = {
            "name": "Màu Trắng",
            "image_url": "https://cdn.shopify.com/s/files/1/0274/5054/0104/products/PDP_0022_December-WHW21SW26-Cloud-NatalieCuffedKnitTop-Fig_6823_cd79e66e-942e-4113-8a7e-56681634008d_400x.jpg?v=1641584642",
            "define": "Màu trắng luôn mang đến người mặc vẻ thanh lịch, trẻ trung và không bao giờ bị lỗi thời",
            "link": "https://vts.edu.vn/6-cach-phoi-mau-thoi-trang-cong-so-hot-nhat/",
        }
        color_list.append(color3)
    
        template_items = []
        for color in color_list:

            template_item = {
                "title": color['name'],
                "image_url": color['image_url'],
                "subtitle": color['define'],
                "default_action": {
                    "type": "web_url",
                    "url": color['link'],
                    "webview_height_ratio": "full"
                },
                "buttons": [
                    {
                        "type": "web_url",
                        "url": color['link'],
                        "title": "Xem ngay"
                    }
                ]
            }
            template_items.append(template_item)

        message_str = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": template_items

                }
            }
        }
        ret_text = "Xin chào! Quý khách có thể tham khảo các màu phối đồ dưới đây:"
        print(message_str)
        dispatcher.utter_message(text=ret_text, json_message=message_str)

        return []