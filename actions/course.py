import json


def get_course(name, attribute):
    try:
        with open("actions/api/course.json", 'r', encoding="utf8") as f:
            data = json.loads(f.read())
            for language in data:
                if language["name"] == str.lower(name):
                    return language[str.lower(attribute)]
    except Exception as e:
        print(str(e))
